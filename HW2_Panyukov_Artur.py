import functools
import sys
import threading
import time


def keyboardinterruptwrapper(func=None, *, sem=None):
    if func is None:
        return lambda func: keyboardinterruptwrapper(func, sem=sem)

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except KeyboardInterrupt:
            sem.release()
            sys.exit(0)
    return wrapper


sem = threading.Semaphore()


@keyboardinterruptwrapper(sem=sem)
def fun1():
    while True:
        sem.acquire()
        print(1)
        sem.release()
        time.sleep(0.25)


@keyboardinterruptwrapper(sem=sem)
def fun2():
    while True:
        sem.acquire()
        print(2)
        sem.release()
        time.sleep(0.25)


t1 = threading.Thread(target=fun1)
t1.start()
t2 = threading.Thread(target=fun2)
t2.start()
