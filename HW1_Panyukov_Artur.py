import psutil


def process_count(username: str) -> int:
    counter = 0
    for proc in psutil.process_iter(attrs=['pid', 'name', 'username']):
        if proc.info['username'] == username:
            counter += 1
    return counter


def total_memory_usage(root_pid: int) -> int:
    mem = 0
    try:
        process = psutil.Process(root_pid)
    except psutil.NoSuchProcess:
        return 0
    mem += process.memory_info()[0]
    for proc in process.children(recursive=True):
        mem += (proc.memory_info()[0] - proc.memory_info()[2])
    return mem
