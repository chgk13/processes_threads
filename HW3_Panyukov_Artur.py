import hashlib
import os
import math
from socket import *


def request_handler(request):
    try:
        number = int(request)
    except Exception:
        return 'Not a number!'.encode()
    fact = math.factorial(number)
    md5_hasher = hashlib.md5()
    md5_hasher.update(str(fact).encode())
    return md5_hasher.digest()


def main_server_function(port: int = 8000, num_of_workers: int = 2):
    '''
    :param port : port number to accept the incoming requests
    :param num_of_workers : number of workers to handle the requests
    '''
    with socket(AF_INET, SOCK_STREAM) as s:
        s.bind(('', port))
        s.listen(1)
        for _ in range(num_of_workers):
            while True:
                c, a = s.accept()
                if os.fork() == 0:
                    c.sendall(b'hello, %s\n' % a[0].encode())
                    msg = c.recv(1000).strip()
                    print('msg: ', msg)
                    result = request_handler(msg)
                    c.sendall(result)
                    c.close()
                    print('Conection closed')
                    os._exit(0)


def main():
    main_server_function(num_of_workers=1)


if __name__ == '__main__':
    main()
